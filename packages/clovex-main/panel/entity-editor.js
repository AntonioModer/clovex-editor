/*
    CloveX Editor - https://bitbucket.org/_AnRu/clovex-editor/src
    Copyright (C) 2016  Andrew Rublyov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

var stage = {}
var grid = {}

var gridStep = 100;
var cameraPosition = {x: 0, y: 0};
var canvasSize = {width: 0, height: 0};
var lastMousePosition = {x: 0, y: 0};
var lastPosition = {x: 0, y: 0};
var mousePressed = false;
var canvas = {}
var mousePosition = {x: 0, y: 0}
var gridCanvas = {}
var gridEnabled = true;
var rulerEnabled = false;
var entity = {}
var selection = []
var selectionTools = {}
var xAxisPressed = false
var yAxisPressed = false

Editor.Panel.extend({
  style: `
    #entity-render {
      width: 100%;
      height: 100%;
    }
    .toolbox {
      padding: 5px;
      vertical-align: middle;
    }
  `,
  $: {
    canvas: '#entity-render',
    gridSizeChange: '#grid-size-change',
    gridEnabledChange: '#grid-enabled-change',
    rulerEnabledChange: '#ruler-enabled-change'
  },
  template: `
    <div class="toolbox">
      <ui-checkbox id="grid-enabled-change" checked>Grid</ui-checkbox>
      <span class="text">Size:</span>
      <ui-num-input id="grid-size-change" 
                    value=100 
                    style="width: 50px;" 
                    min=1 max=10000 step=10 
                    type="int"></ui-num-input>
      <ui-checkbox id="ruler-enabled-change">Ruler</ui-checkbox>
    </div>
    <canvas id="entity-render" style="background: rgb(50,50,50)"></canvas>
  `,
  ready () {
    canvas = this.$canvas;
    stage = new createjs.Stage(this.$canvas);
    init();

    this.$gridSizeChange.addEventListener("change", function (e) {
      gridStep = e.target.value
      redrawCanvas()
    });
    this.$gridEnabledChange.addEventListener("change", function (e) {
      gridEnabled = e.target.value
      redrawCanvas()
    });
    this.$rulerEnabledChange.addEventListener("change", function (e) {
      rulerEnabled = e.target.value
      redrawCanvas()
    })

    // Mouse dragging
    this.$canvas.addEventListener("mousedown", function (e) {
      lastMousePosition.x = stage.mouseX
      lastMousePosition.y = stage.mouseY
      if (e.button != 2) return
      mousePressed = true

    })
    this.$canvas.addEventListener("mousemove", function (e) {
      if (mousePressed) {
        let diffX = stage.mouseX - lastMousePosition.x
        let diffY = stage.mouseY - lastMousePosition.y

        cameraPosition.x += diffX
        cameraPosition.y += diffY

        stage.setTransform(cameraPosition.x, cameraPosition.y)

        lastMousePosition.x = stage.mouseX
        lastMousePosition.y = stage.mouseY
        redrawCanvas()
      }
      if (xAxisPressed) {
        let diffX = stage.mouseX - lastMousePosition.x

        selection[0].x += diffX

        lastMousePosition.x = stage.mouseX
        redrawCanvas()
      }
      if (yAxisPressed) {
        let diffY = stage.mouseY - lastMousePosition.y

        selection[0].y += diffY

        lastMousePosition.y = stage.mouseY
        redrawCanvas()
      }
    })
    this.$canvas.addEventListener("mouseup", function (e) {
      xAxisPressed = false
      yAxisPressed = false
      if (e.button != 2) return
      mousePressed = false
    })

  },
  listeners: {
    'panel-resize' () {
      if ( true ) {
        window.requestAnimationFrame(() => {
          let rect = this.$canvas.getBoundingClientRect();
          updateCanvasSize(rect.width, rect.height)
          redrawCanvas()
        });
      }
    }
  }
});


function init() {
  stage.setTransform(cameraPosition.x, cameraPosition.y)

  // Initialize entity
  entity = newEntity('app://test.png')
  entity.x = 200
  entity.y = 50
  //entity.getChildByName("children").addChild(test)
  stage.addChild(entity)

  selectionTools = initTransformTools()
  stage.addChild(selectionTools)

  createGrid()
}

function createGrid() {
  updateGrid()
}

function updateGrid() {
  var ctx = canvas.getContext('2d')
  ctx.clearRect(0, 0, canvasSize.x, canvasSize.y)

  let startX = Math.round(-cameraPosition.x / gridStep)
  let startY = Math.round(-cameraPosition.y / gridStep) 

  let finishX = startX + Math.round(canvasSize.width / gridStep) 
  let finishY = startY + Math.round(canvasSize.height / gridStep) 

  ctx.lineWidth=1;
  ctx.strokeStyle = "rgb(100, 100, 100)";
  for (var i=startX; i<=finishX; i++) { 
    ctx.beginPath();
    ctx.moveTo(cameraPosition.x + i * gridStep, 0)
    ctx.lineTo(cameraPosition.x + i * gridStep, canvasSize.height)
    ctx.stroke();
    if (rulerEnabled) ctx.strokeText(i * gridStep, cameraPosition.x + i * gridStep + 5, 15) 
  } 

  for (var i=startY; i<=finishY; i++) { 
    ctx.beginPath();
    ctx.moveTo(0, cameraPosition.y + i * gridStep)
    ctx.lineTo(canvasSize.width, cameraPosition.y + i * gridStep)
    ctx.stroke();
    if (rulerEnabled) ctx.strokeText(i * gridStep, 5, cameraPosition.y + i * gridStep) 
  }
}
function updateCanvasSize(w, h) {
  canvasSize.width = w;
  canvasSize.height = h;
  canvas.setAttribute("width", canvasSize.width);
  canvas.setAttribute("height", canvasSize.height);
}

function redrawCanvas() {
  stage.update()
  if (gridEnabled) updateGrid()
  updateGizmos()
}


function updateGizmos() {
  redrawSelection(entity)
}

function redrawSelection(o) {
  console.log("Selection: ")
  if (selection.indexOf(o) != -1) {
    let globalpos = o.localToGlobal(0, 0)
    selectionTools.x = globalpos.x - cameraPosition.x
    selectionTools.y = globalpos.y - cameraPosition.y

    var ctx = canvas.getContext('2d')
    ctx.strokeStyle = "#0F0";
    let globalPos = o.localToGlobal(0, 0)
    let bounds = o.getChildByName("graphics").getBounds()
    ctx.strokeRect(globalPos.x, globalPos.y, bounds.width, bounds.height)
    return
  }

  let children = o.getChildByName("children")

  for (var i in children.children) {
    redrawSelection(children.children[i])
  }
}

function newEntity(image, parent) {
  let ent = new createjs.Container()
  let children = new createjs.Container()
  let graphics = new createjs.Bitmap(image)
  graphics.name = "graphics"
  children.name = "children"
  graphics.image.onload = function(e) { 
    let width = e.target.width
    let height = e.target.width
    // Draw selection when image will be re-/loaded
    redrawCanvas() 
  }
  ent.addChild(graphics)
  ent.addChild(children)
  ent.addEventListener("click", function(e) {
    selectMe(e.target.parent)
    redrawCanvas()
  })
  if (parent == null || parent == stage) {
    return stage.addChild(ent)
  }
    return parent.getChildByName("children").addChild(ent)
}

function selectMe(e) {
  selection[0] = e
  stage.setChildIndex(selectionTools, stage.getNumChildren()-1);
  selectionTools.visible = true
}

function initTransformTools() {
  let positionAxis = new createjs.Container()

  // X axis arrow
  let xArrow = new createjs.Shape()
  positionAxis.addChild(xArrow)
  // X axis graphics
      xArrow.graphics.beginStroke("#F00").setStrokeStyle(3)
      xArrow.graphics.moveTo(0, 0).lineTo(100, 0).moveTo(100, 0).lineTo(90, 5).moveTo(100, 0).lineTo(90, -5)

  // now we need to add hit area to the xAxis
  let xHit = new createjs.Shape();
      xHit.graphics.beginFill("#000").drawRect(0, -5, 100, 10)
      xArrow.hitArea = xHit

  // Events
  xArrow.addEventListener("mousedown", function() {
    xAxisPressed = true
  })

  // Y axis arrow
  let yArrow = new createjs.Shape()
  positionAxis.addChild(yArrow)
  // Y axis graphics
      yArrow.graphics.beginStroke("#0F0").setStrokeStyle(3)
      yArrow.graphics.moveTo(0, 0).lineTo(0, 100).moveTo(0, 100).lineTo(5, 90).moveTo(0, 100).lineTo(-5, 90)

  // now we need to add hit area to the yAxis
  let yHit = new createjs.Shape();
      yHit.graphics.beginFill("#000").drawRect(-5, 0, 10, 100)
      yArrow.hitArea = yHit

  // Events
  yArrow.addEventListener("mousedown", function() {
    yAxisPressed = true
  })

  // Square
  let square = new createjs.Shape()
  positionAxis.addChild(square)

  square.graphics.beginStroke("#FF0").beginFill("rgba(255, 255, 0, .25)").rect(0, 0, 32, 32)

  // Events
  square.addEventListener("mousedown", function() {
    xAxisPressed = true
    yAxisPressed = true
  })

  // now we will add new childs
  positionAxis.visible = false
  return positionAxis
}